# qp_plugins

Programs developed for the [Quantum Package](http://github.com/LCPQ/quantum_package).

* `/stable` : Plugins that are stable and maintained
* `/devel`  : Experimental plugins under development
* `/deprecated` : Old plugins that have been abandoned and are not maintained


