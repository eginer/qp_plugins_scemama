program mpn
  implicit none
  BEGIN_DOC
! TODO : Put the documentation of the program here
  END_DOC
  integer :: i, k, l
  double precision, allocatable :: c_pert(:,:)
  double precision, allocatable :: e_pert(:)
  double precision, allocatable :: hc(:), s2(:)

  n_states_diag = 1
  TOUCH n_states_diag
  call generate_fci_space
  allocate(c_pert(N_det,0:mp_order))
  allocate(s2(N_det))
  allocate(e_pert(0:mp_order))
  e_pert(0) = energy_det_i(1)
  c_pert(:,:) = 0.d0
  c_pert(1,0) = 1.d0

  e_pert(1) = hf_energy - e_pert(0) - nuclear_repulsion
  do k=1,mp_order
    ! H_ij C^(k-1)
    if (distributed_davidson) then
        call H_S2_u_0_nstates_zmq   (c_pert(1,k),s2,c_pert(1,k-1),1,N_det)
    else
        call H_S2_u_0_nstates_openmp(c_pert(1,k),s2,c_pert(1,k-1),1,N_det)
    endif
    if (k>1) then
      e_pert(k) += c_pert(1,k)
    endif
    print *, k, e_pert(k), sum(e_pert) + nuclear_repulsion

    c_pert(:,k) = -c_pert(:,k)
    c_pert(1,k) = 0.d0
    do l=1,k
      do i=2,N_det
        c_pert(i,k) = c_pert(i,k) + e_pert(l) * c_pert(i,k-l)
      enddo
    enddo
    do i=2,N_det
      c_pert(i,k) = c_pert(i,k) + energy_det_i(i) * c_pert(i,k-1)
    enddo
    do i=2,N_det
      c_pert(i,k) = c_pert(i,k) / (energy_det_i(i) - energy_det_i(1))
    enddo

  enddo


end


