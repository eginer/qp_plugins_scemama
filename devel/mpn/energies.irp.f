BEGIN_PROVIDER [ double precision, energy_det_i, (N_det) ]
 implicit none
 BEGIN_DOC
 ! Fock Energy of determinant |I> (sum of epsilon_i) 
 END_DOC
 integer :: i, k, n
 integer :: list(elec_alpha_num)

 do k=1,N_det
   call bitstring_to_list(psi_det(1,1,k), list, n, N_int)
   energy_det_i(k) = 0.d0
   do i=1,n
     energy_det_i(k) += fock_matrix_diag_mo(list(i))
   enddo
   call bitstring_to_list(psi_det(1,2,k), list, n, N_int)
   do i=1,n
     energy_det_i(k) += fock_matrix_diag_mo(list(i))
   enddo
 enddo
END_PROVIDER



