BEGIN_PROVIDER [ double precision, tau_cc, (spin_occ_num,spin_occ_num,spin_vir_num,spin_vir_num) ]
 implicit none
 BEGIN_DOC
 ! Tau in CCSD
 END_DOC

  integer                       :: i,j,a,b

  do b=1,spin_vir_num
    do a=1,spin_vir_num
      do j=1,spin_occ_num
        do i=1,spin_occ_num

          tau_cc(i,j,a,b) = t2_cc(i,j,a,b) + t1_cc(i,a)*t1_cc(j,b) - t1_cc(i,b)*t1_cc(j,a)

        enddo
      enddo
    enddo
  enddo

END_PROVIDER
