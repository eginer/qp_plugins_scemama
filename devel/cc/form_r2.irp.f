subroutine form_r2(gvv,goo,aoooo,bvvvv,hovvo,t1,t2,tau,r2)

! Form tau in CCSD

  implicit none

! Input variables

  double precision,intent(in)   :: gvv(spin_vir_num,spin_vir_num)
  double precision,intent(in)   :: goo(spin_occ_num,spin_occ_num)
  double precision,intent(in)   :: aoooo(spin_occ_num,spin_occ_num,spin_occ_num,spin_occ_num)
  double precision,intent(in)   :: bvvvv(spin_vir_num,spin_vir_num,spin_vir_num,spin_vir_num)
  double precision,intent(in)   :: hovvo(spin_occ_num,spin_vir_num,spin_vir_num,spin_occ_num)

  double precision,intent(in)   :: t1(spin_occ_num,spin_vir_num)
  double precision,intent(in)   :: t2(spin_occ_num,spin_occ_num,spin_vir_num,spin_vir_num)
  double precision,intent(in)   :: tau(spin_occ_num,spin_occ_num,spin_vir_num,spin_vir_num)

! Local variables

  integer                       :: i,j,k,l
  integer                       :: a,b,c,d

! Output variables

  double precision,intent(out)  :: r2(spin_occ_num,spin_occ_num,spin_vir_num,spin_vir_num)

  r2(:,:,:,:) = OOVV(:,:,:,:)

  !$OMP PARALLEL DO DEFAULT(NONE) &
  !$OMP SHARED(spin_occ_num,spin_vir_num,r2,aoooo,bvvvv,gvv,goo,tau,OVOO,OVVV,t1,t2,hovvo,OVVO) &
  !$OMP PRIVATE(i,j,a,b,k,l,c,d)
  do b=1,spin_vir_num
    do a=1,spin_vir_num
      do j=1,spin_occ_num
        do i=1,spin_occ_num

          do k=1,spin_occ_num
            do l=1,spin_occ_num
              r2(i,j,a,b) = r2(i,j,a,b) + aoooo(i,j,k,l)*tau(k,l,a,b)
            end do
          end do

          do d=1,spin_vir_num
            do c=1,spin_vir_num
              r2(i,j,a,b) = r2(i,j,a,b) + bvvvv(c,d,a,b)*tau(i,j,c,d)
            end do
          end do

          do c=1,spin_vir_num
            r2(i,j,a,b) = r2(i,j,a,b) + gvv(c,a)*t2(i,j,c,b)
          end do

          do k=1,spin_occ_num
            r2(i,j,a,b) = r2(i,j,a,b) + OVOO(k,a,i,j)*t1(k,b)
          end do

          do c=1,spin_vir_num
            r2(i,j,a,b) = r2(i,j,a,b) - gvv(c,b)*t2(i,j,c,a)
          end do

          do k=1,spin_occ_num
            r2(i,j,a,b) = r2(i,j,a,b) - OVOO(k,b,i,j)*t1(k,a)
          end do

          do k=1,spin_occ_num
            r2(i,j,a,b) = r2(i,j,a,b) - goo(i,k)*t2(k,j,a,b)
          end do

          do c=1,spin_vir_num
            r2(i,j,a,b) = r2(i,j,a,b) + OVVV(j,c,b,a)*t1(i,c)
          end do

          do k=1,spin_occ_num
            r2(i,j,a,b) = r2(i,j,a,b) + goo(j,k)*t2(k,i,a,b)
          end do

          do c=1,spin_vir_num
            r2(i,j,a,b) = r2(i,j,a,b) - OVVV(i,c,b,a)*t1(j,c)
          end do

          do k=1,spin_occ_num
            do c=1,spin_vir_num
              r2(i,j,a,b) = r2(i,j,a,b) + hovvo(i,c,a,k)*t2(j,k,b,c)
            end do
          end do

          do k=1,spin_occ_num
            do c=1,spin_vir_num
              r2(i,j,a,b) = r2(i,j,a,b) - OVVO(i,c,a,k)*t1(j,c)*t1(k,b)
            end do
          end do

          do k=1,spin_occ_num
            do c=1,spin_vir_num
              r2(i,j,a,b) = r2(i,j,a,b) - hovvo(j,c,a,k)*t2(i,k,b,c)
            end do
          end do

          do c=1,spin_vir_num
            do k=1,spin_occ_num
               r2(i,j,a,b) = r2(i,j,a,b) + OVVO(j,c,a,k)*t1(i,c)*t1(k,b)
            end do
          end do

          do c=1,spin_vir_num
            do k=1,spin_occ_num
              r2(i,j,a,b) = r2(i,j,a,b) - hovvo(i,c,b,k)*t2(j,k,a,c)
            end do
          end do

          do c=1,spin_vir_num
            do k=1,spin_occ_num
              r2(i,j,a,b) = r2(i,j,a,b) + OVVO(i,c,b,k)*t1(j,c)*t1(k,a)
            end do
          end do

          do c=1,spin_vir_num
            do k=1,spin_occ_num
              r2(i,j,a,b) = r2(i,j,a,b) + hovvo(j,c,b,k)*t2(i,k,a,c)
            end do
          end do

          do c=1,spin_vir_num
            do k=1,spin_occ_num
              r2(i,j,a,b) = r2(i,j,a,b) - OVVO(j,c,b,k)*t1(i,c)*t1(k,a)
            end do
          end do

        end do
      end do
    end do
  end do
  !$OMP END PARALLEL DO

end subroutine form_r2
