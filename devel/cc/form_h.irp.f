subroutine form_h(t1,tau,hvv,hoo,hvo)

! Scuseria Eqs. (5), (6) and (7)

  implicit none

! Input variables

  double precision,intent(in)   :: t1(spin_occ_num,spin_vir_num)
  double precision,intent(in)   :: tau(spin_occ_num,spin_occ_num,spin_vir_num,spin_vir_num)

! Local variables

  integer                       :: i,j,k,l
  integer                       :: a,b,c,d

! Output variables

  double precision,intent(out)  :: hvv(spin_vir_num,spin_vir_num)
  double precision,intent(out)  :: hoo(spin_occ_num,spin_occ_num)
  double precision,intent(out)  :: hvo(spin_vir_num,spin_occ_num)

  hvv(:,:) = 0d0

  do b=1,spin_vir_num 
    hvv(b,b) = eV(b)
    do a=1,spin_vir_num
      do j=1,spin_occ_num
        do k=1,spin_occ_num
          do c=1,spin_vir_num

            hvv(b,a) = hvv(b,a) - OOVV(j,k,b,c)*tau(j,k,a,c)

          end do
        end do
      end do
    end do
  end do

  hoo(:,:) = 0d0

  do i=1,spin_occ_num
    hoo(i,i) = eO(i)
    do j=1,spin_occ_num
      do k=1,spin_occ_num
        do b=1,spin_vir_num
          do c=1,spin_vir_num

            hoo(i,j) = hoo(i,j) + OOVV(j,k,b,c)*tau(i,k,b,c)

          end do
        end do
      end do
    end do
  end do

  hvo(:,:) = 0d0

  do j=1,spin_occ_num 
    do b=1,spin_vir_num
      do c=1,spin_vir_num
        do k=1,spin_occ_num

          hvo(b,j) = hvo(b,j) + OOVV(j,k,b,c)*t1(k,c)

        end do
      end do
    end do
  end do

! print*,'hvv',hvv

end subroutine form_h
