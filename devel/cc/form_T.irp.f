subroutine form_T(ub,ubb,EcCCT)

! Compute (T) correction

  implicit none

! Input variables

  double precision,intent(in)   :: ub(spin_occ_num,spin_occ_num,spin_occ_num,spin_vir_num,spin_vir_num,spin_vir_num)
  double precision,intent(in)   :: ubb(spin_occ_num,spin_occ_num,spin_occ_num,spin_vir_num,spin_vir_num,spin_vir_num)

! Local variables

  integer                       :: i,j,k,l
  integer                       :: a,b,c,d

! Output variables

  double precision,intent(out)  :: EcCCT

  EcCCT = 0d0

  do c=1,spin_vir_num
    do b=1,spin_vir_num
      do a=1,spin_vir_num
        do k=1,spin_occ_num
          do j=1,spin_occ_num
            do i=1,spin_occ_num

              EcCCT = EcCCT                                &
                    + (ub(i,j,k,a,b,c) + ubb(i,j,k,a,b,c)) &
                    * ubb(i,j,k,a,b,c)/delta_OOOVVV(i,j,k,a,b,c) 

            end do
          end do
        end do
      end do
    end do
  end do

  EcCCT = - EcCCT/36d0

end subroutine form_T
