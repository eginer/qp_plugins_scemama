program CC
  implicit none
  BEGIN_DOC
  ! CC
  ! Coupled cluster
  ! 
  ! This program perform coupled cluster calculation
  ! CCSD or CCSD(T)
  END_DOC
  call CCSD
end
