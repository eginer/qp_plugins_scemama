 BEGIN_PROVIDER [ double precision, dressing_column_h, (N_det,N_states) ]
&BEGIN_PROVIDER [ double precision, dressing_column_s, (N_det,N_states) ]
 implicit none
 BEGIN_DOC
 ! \Delta_{state-specific}. \Psi
 ! Diagonal element is divided by 2 because Delta = D + D^t
 END_DOC

 integer :: i,ii,k,j, l
 double precision :: f, tmp
 double precision, allocatable :: delta(:)

 allocate(delta(N_det))
 delta(1:N_det) = dmc_delta_h(1:N_det)

 call dset_order(delta,psi_bilinear_matrix_order_reverse,N_det)

 dressing_column_h(:,:) = 0.d0
 dressing_column_s(:,:) = 0.d0

 l = dressed_column_idx(1)
 do j = 1, n_det
   if (j == l) cycle
   dressing_column_h(j,1)  = delta(j) 
   dressing_column_h(l,1) -= psi_coef(j,1) * delta(j) / psi_coef(l,1)
 enddo
 dressing_column_h(l,1) += delta(l) 
 dressing_column_h(l,1) *= 0.5d0

END_PROVIDER



