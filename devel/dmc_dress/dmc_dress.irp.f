program dmc_dress
 implicit none
 BEGIN_DOC
! Program that extracts the lowest states of the Hamiltonian dressed by the QMC
! dressing vector stored in :option:`dmc_dressing dmc_delta_h`
!
 END_DOC
 read_wf = .True.
 touch read_wf
! call pre
 call routine
 call save_wavefunction_general(N_det,N_states,psi_det,size(psi_coef,1),psi_coef)
end

subroutine pre
 implicit none
 double precision, allocatable :: left(:,:), right(:,:), tmp(:,:), res(:,:)
 integer :: i
 allocate (left(1,1:N_det), right(1:N_det,1), tmp(1:N_det,1), res(1,1))
 left(1,1:N_det) = psi_coef(1:N_det,1)
 right(1:N_det,1) = psi_coef(1:N_det,1)

 tmp(1:N_det,1:1) = matmul(h_matrix_dressed(1:N_det,1:N_det), right(1:N_det,1:1))
 res(1:1,1:1) = matmul(left(1:1,1:N_det), tmp(1:N_det,1:1))
 print *,  'E_in = ', res(1,1)
 do i=1,N_det
 print *,  'HPsi/c0 = ', tmp(i,1)/psi_coef(i,1)  
 enddo
end
subroutine routine
 implicit none
 psi_coef(1:N_det,1) = ci_eigenvectors_dressed(1:N_det,1)
 print*,'N_det = ',N_det
 print *, 'E = ', ci_energy_dressed(1)
 SOFT_TOUCH psi_coef
end
