#!/usr/bin/env python

import numpy as np
import subprocess
import sys
import os

QP_PATH=os.environ["QP_ROOT"]+"/external/ezfio/Python/"

sys.path.insert(0,QP_PATH)

from ezfio import ezfio

def read_hamiltonian(inp):
    text = subprocess.run(["qmcchem", "result", inp], capture_output=True).stdout
    inside = None
    h = []
    s = []
    norm = None
    for line in text.splitlines():
       line = str(line)
       print (line)
       if "Psi_norm :" in line:
           norm = float(line.split()[3])
       if "]" in line:
           inside = None
       if inside == "H":
          data = line.split()
          h.append(float(data[3]))
       elif "Ci_dress" in line:
           inside = "H"
    h = np.array(h)/norm

    return h


h = read_hamiltonian(sys.argv[1])
ezfio.set_file(sys.argv[1])
ezfio.set_dmc_dress_dmc_delta_h(h)
print(h)
