===================
import_integrals_ao
===================

Module to read all the integrals in the |AO| basis from files (all in atomic units).


The following files are required:

- :file:`S.qp` : overlap integrals
- :file:`T.qp` : kinetic integrals
- :file:`V.qp` : electron-nucleus potential integrals
- :file:`P.qp` : pseudo-potential integrals
- :file:`W.qp` : electron repulsion integrals

If present, the :file:`E.qp` file, should contain the nuclear repulsion energy.

In all the other files, there is one integral per line and for the one-electron integral
$\int \chi_i(r) \hat{O} \chi_j(r) dr$, the format is

  i j value

and for two electron integral the format uses the physicists' convention,
$\int \chi_i(r_1) \chi_j(r_2) \hat{O} \chi_k(r_1) \chi_l(r_2) dr$:

  i j k l value


