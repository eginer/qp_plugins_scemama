program e_curve
  use bitmasks
  implicit none
  integer :: i,j,k, kk, nab, m, l
  double precision :: norm, E, hij, num, ci, cj
  double precision :: e_0(N_states)
  double precision, allocatable :: psi_save(:)
  PROVIDE mo_two_e_integrals_in_map mo_one_e_integrals

  if (.not.read_wf) then
    stop 'Please set read_wf to true'
  endif

  PROVIDE psi_bilinear_matrix nuclear_repulsion
  PROVIDE psi_coef_sorted psi_det psi_coef
  print *,  ''
  print *,  '=============================='
  print *,  'Energies at different cut-offs'
  print *,  '=============================='
  print *,  ''
  print *,  '=========================================================='
  print '(A8,2X,A8,2X,A12,2X,A10,2X,A12)',  'Thresh.', 'Ndet', 'Cost', 'Norm', 'E'
  print *,  '=========================================================='
  double precision :: thresh
  integer(bit_kind), allocatable :: det_i(:,:), det_j(:,:)
  thresh = 1.d-10
  integer :: n_det_prime

  nab = n_det_alpha_unique+n_det_beta_unique

  allocate(psi_save(1:N_det))
  psi_save(1:N_det) = psi_coef(1:N_det,1)
  do while (thresh  < 1.d0)
    norm = 0.d0
    n_det_prime = n_det
    do k=1,n_det
      psi_coef(k,1) = psi_save(k)
      if (dabs(psi_coef(k,1)) < thresh) then
        psi_coef(k,1) = 0.d0
        n_det_prime -= 1
      endif
      norm = norm + psi_coef(k,1)**2
    enddo
    TOUCH psi_coef psi_det
    norm = norm/dsqrt(norm)
    psi_coef(:,1) = psi_coef(:,1)/norm

    integer :: na, nb
    na = 0
    do k=1,n_det_alpha_unique
      if (det_alpha_norm(k) > 0.d0) then
        na += 1
      endif
    enddo
    nb = 0
    do k=1,n_det_beta_unique
      if (det_beta_norm(k) > 0.d0) then
        nb += 1
      endif
    enddo
    E = psi_energy(1)
    double precision :: cost0, cost
    cost0 = elec_alpha_num**3 + elec_beta_num**3
    cost = (na-1) * elec_alpha_num**2 + &
           (nb-1) * elec_beta_num**2 + &
            elec_alpha_num**3 + elec_beta_num**3
    cost = cost/cost0
    print '(E9.1,2X,I8,2X,F10.2,2X,F10.8,2X,F15.10)',  thresh, n_det_prime, &
      cost, norm, E
    thresh = thresh * dsqrt(10.d0)
  enddo
  print *,  '=========================================================='

end

